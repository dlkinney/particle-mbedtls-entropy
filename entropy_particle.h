/**
 * \file entropy_particle.h
 *
 * \brief
 * Provides a hardware-based strong entropy source for the mbedTLS entropy
 * collector.
 *
 * \author David L Kinney <david@pinkhop.com>
 *
 * \copyright
 * Copyright (C) 2016, David L Kinney, All Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stddef.h>

/**
 \brief
 API to use for accessing the hardware random number generator ("RNG").

 \discussion
 There are multiple APIs available to access the values produced by the
 hardware RNG.

 Possible values are:
 - \c PARTICLE_HAL
 - \c STM32F2XX_STDPERIPHDRIVER
 - \c NONE

 \c PARTICLE_HAL will cause the compiler to use the RNG API defined in the
 Particle hardware abstraction layer ("HAL"). The Particle HAL is part of the
 the Particle firmware and licensed under the GPLv3 (as of May 2016). Since
 this API is part of a HAL, one might reasonably expect that future Particle
 devices with hardware RNG will implement this API and continue to work as-is.

 \c STM32F2XX_STDPERIPHDRIVER will cause the compiler to use the RNG API from
 the STM32F2XX Standard Peripheral Drivers. The Standard Peripheral Drivers are
 licensed under the MCD-ST Liberty SW License Agreement V2 (found at
 http://goo.gl/4yvxt5 as of May 2016).

 \c NONE will cause the compiler to generate a no-op implementation of the \c
 entropy_source_particle() function.

 The Photon, P0, and Electron all use the STM32F205 chipset, which has a
 hardware random number generator. The Core uses the STM32F103CB chip, which
 does not have a hardware random number generator.
 */
#define ENTROPY_PARTICLE_SOURCE PARTICLE_HAL

/**
 \brief
 Implements the mbedTLS API for an entropy for the mbedTLS entropy collector.

 \discussion
 Populates \c output with at most \c len bytes of random data. Sets \c olen to
 the actual number of bytes generated.

 \param data
 Callback-specific data pointer. Ignored by the current implementation. Can be
 \c NULL .

 \param output
 Data to fill.

 \param len
 Maximum size to provide.

 \param olen
 The actual amount of bytes put into the buffer. Can be \c 0 .

 \return
 \c 0 if no critical failures occurred; \c MBEDTLS_ERR_ENTROPY_SOURCE_FAILED
 otherwise.
 */
int entropy_source_particle(void *data, unsigned char *output, size_t len, size_t *olen);
