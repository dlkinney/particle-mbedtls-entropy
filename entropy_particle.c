/**
 * \file entropy_particle.c
 *
 * \brief
 * Provides a hardware-based strong entropy source for the mbedTLS entropy
 * collector.
 *
 * \author David L Kinney <david@pinkhop.com>
 *
 * \copyright
 * Copyright (C) 2016, David L Kinney, All Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "entropy_particle.h"

#include <stdbool.h>
#include <stdint.h>

#include "entropy.h"


#if ENTROPY_PARTICLE_SOURCE == PARTICLE_HAL

// Particle Hardware Abstraction Layer
#include "application.h"
#define _particle_entropy_init() HAL_RNG_Configuration()
#define _particle_entropy_rand(var_ptr) *var_ptr = HAL_RNG_GetRandomNumber()

#elif ENTROPY_PARTICLE_SOURCE == STM32F2XX_STDPERIPHDRIVER

// STM32F2XX Standard Peripheral Drivers
#include "stm32f2xx_rng.h"
#include "stm32f2xx_rcc.h"
#define _particle_entropy_init() if (1) { RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE); RNG_Cmd(ENABLE); }
#define _particle_entropy_rand(var_ptr) if (1) { while (RNG_GetFlagStatus(RNG_FLAG_DRDY) != SET); *var_ptr = RNG_GetRandomNumber(); }

#endif


#if defined(_particle_entropy_init) && defined(_particle_entropy_rand)

static bool _particle_entropy_active = false;

uint8_t _entropy_particle_next_byte() {
    #define BUFFER_SIZE 4 /* sizeof(uint32_t) */

    static uint8_t buffer[BUFFER_SIZE] = { 0 };
    static size_t buffer_index = BUFFER_SIZE;  // start past end of the array

    // activate the hardware needed for
    if (! _particle_entropy_active) {
        _particle_entropy_init();

        // note that we have started the RNG hardware so we do not start it
        // again later
        _particle_entropy_active = true;
    }

    // re-populate the entropy buffer if it has been exhausted
    if (buffer_index >= BUFFER_SIZE) {

        // wait until 32 bits of entropy are available
        while (RNG_GetFlagStatus(RNG_FLAG_DRDY) != SET) { }

        // read the 32 bits of entropy
        uint32_t random_number = 0;
        _particle_entropy_rand(&random_number);

        // update the buffer
        buffer[0] = 0xff & (random_number);
        buffer[1] = 0xff & (random_number >>  8);
        buffer[2] = 0xff & (random_number >> 16);
        buffer[3] = 0xff & (random_number >> 24);

        // reset the index
        buffer_index = 0;
    }

    // read the next byte from the entropy buffer
    uint8_t next_byte = buffer[buffer_index];

    // move the index to the next unread byte of entropy
    ++buffer_index;

    return next_byte;
}

int entropy_source_particle(void *data, unsigned char *output, size_t len, size_t *olen) {
    for (size_t i = 0; i < len; ++i) {
        output[i] = _entropy_particle_next_byte();
    }
    *olen = len;

    return 0; // indicate that the entropy was successfully provided
}

#else /* defined(_particle_entropy_init) && defined(_particle_entropy_rand) */

int entropy_source_particle(void *data, unsigned char *output, size_t len, size_t *olen) {
    return MBEDTLS_ERR_ENTROPY_SOURCE_FAILED;
}

#endif /* defined(_particle_entropy_init) && defined(_particle_entropy_rand) */
